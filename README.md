# entertainment-wallboard

A status wallboard for my home entertainment system

## Current state

This is a WIP, still in active development

## Developing

1. Launch

```bash
make shell # Launch a bash shell inside the container
make serve # Start & run the atlasboard server
```

2. Then, go to `localhost:3000/entertainment-wallboard`

