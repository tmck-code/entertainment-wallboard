#!/bin/bash

set -euxo pipefail


atlasboard new entertainment
cd entertainment
npm install

atlasboard generate job upcoming-job
atlasboard generate widget upcoming-widget
atlasboard generate dashboard entertainment-dashboard 

