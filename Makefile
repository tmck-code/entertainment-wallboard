.PHONY: build
build:
	docker build -f ops/Dockerfile -t wallboard .

.PHONY: serve
serve: build
	docker run -it \
    -p "3000:3000" \
    -v $(shell pwd):/wallboard \
    wallboard:latest \
    bash -c "./ops/bootstrap.sh && cd entertainment && atlasboard start 3000"

.PHONY: shell
shell: build
	docker run -it \
    -p "3000:3000" \
    -v $(shell pwd):/wallboard \
    wallboard:latest \
    bash

